from aquamobil import parser as aparser
from chebarkul import parser as cparser
from gorny import parser as gparser
from lubima import parser as lparser
from vlasov import parser as vparser
from zhivaya import parser as zparser
from prettytable import PrettyTable
funclist = [aparser, cparser, gparser, lparser, vparser, zparser]
complist = ["aquamobil", "chebarkulsky istok", "gorny", "lubima", "vlasov kluch", "zhivaya kaplya"]
pricelist = []
for func in funclist:
    pricelist.append(int(func()))
outTable = PrettyTable()
outTable.field_names = [' '] + complist
outTable.add_row(['price'] + pricelist)
print(outTable)
